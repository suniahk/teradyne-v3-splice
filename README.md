*** Credits ***
 - Design created by Splice Digital
 - HTML and CSS conversion created by Alex Watson, 2/27/2019
 - Bootstrap 4.4 from https://getbootstrap.com

*** Next Steps ***
 - Implementing a proper build system for css and icon images.

*** Other Notes ***
 - Some elements, specifically the mouseover colour overlay on the application icons seems to be misaligned in the psd.
 I've created them here properly aligned, but this is something that inter-office communication would make trivial.